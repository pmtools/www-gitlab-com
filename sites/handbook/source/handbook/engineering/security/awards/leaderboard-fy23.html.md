---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 1700 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1510 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1100 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 980 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 940 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 7 | 900 |
| [@brodock](https://gitlab.com/brodock) | 8 | 800 |
| [@kassio](https://gitlab.com/kassio) | 9 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 10 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 11 | 600 |
| [@.luke](https://gitlab.com/.luke) | 12 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 13 | 540 |
| [@ifarkas](https://gitlab.com/ifarkas) | 14 | 520 |
| [@ratchade](https://gitlab.com/ratchade) | 15 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 16 | 500 |
| [@jlear](https://gitlab.com/jlear) | 17 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 18 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 19 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 20 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 21 | 500 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 22 | 500 |
| [@toupeira](https://gitlab.com/toupeira) | 23 | 480 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 480 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 25 | 460 |
| [@garyh](https://gitlab.com/garyh) | 26 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 28 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 29 | 400 |
| [@xanf](https://gitlab.com/xanf) | 30 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 31 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 32 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 33 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 34 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 35 | 300 |
| [@vshushlin](https://gitlab.com/vshushlin) | 36 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 37 | 220 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 38 | 210 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 39 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 40 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 41 | 190 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 42 | 190 |
| [@10io](https://gitlab.com/10io) | 43 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 44 | 140 |
| [@avielle](https://gitlab.com/avielle) | 45 | 140 |
| [@alexpooley](https://gitlab.com/alexpooley) | 46 | 130 |
| [@egrieff](https://gitlab.com/egrieff) | 47 | 120 |
| [@lauraX](https://gitlab.com/lauraX) | 48 | 120 |
| [@dblessing](https://gitlab.com/dblessing) | 49 | 120 |
| [@mattkasa](https://gitlab.com/mattkasa) | 50 | 100 |
| [@mwoolf](https://gitlab.com/mwoolf) | 51 | 90 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 52 | 90 |
| [@drew](https://gitlab.com/drew) | 53 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 54 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 55 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 56 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 57 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 58 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 59 | 60 |
| [@minac](https://gitlab.com/minac) | 60 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 61 | 60 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 62 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 63 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 64 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 65 | 60 |
| [@cngo](https://gitlab.com/cngo) | 66 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 67 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 68 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 69 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 70 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 71 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 72 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 73 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 74 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 75 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 76 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 77 | 30 |
| [@subashis](https://gitlab.com/subashis) | 78 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 79 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 80 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 81 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 82 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 83 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 84 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 85 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 86 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@greg](https://gitlab.com/greg) | 2 | 500 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 3 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 5 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 6 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 7 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 8 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 9 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 10 | 30 |
| [@fneill](https://gitlab.com/fneill) | 11 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 2 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 3 | 400 |
| [@vburton](https://gitlab.com/vburton) | 4 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |
| [@trakos](https://gitlab.com/trakos) | 6 | 200 |

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@sabrams](https://gitlab.com/sabrams) | 1 | 1100 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 2 | 900 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 500 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 4 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 480 |
| [@ifarkas](https://gitlab.com/ifarkas) | 6 | 420 |
| [@viktomas](https://gitlab.com/viktomas) | 7 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 8 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 9 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 10 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 11 | 240 |
| [@vshushlin](https://gitlab.com/vshushlin) | 12 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 13 | 220 |
| [@brodock](https://gitlab.com/brodock) | 14 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 15 | 200 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 16 | 180 |
| [@avielle](https://gitlab.com/avielle) | 17 | 140 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 18 | 130 |
| [@drew](https://gitlab.com/drew) | 19 | 90 |
| [@kerrizor](https://gitlab.com/kerrizor) | 20 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 21 | 80 |
| [@10io](https://gitlab.com/10io) | 22 | 70 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 23 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 24 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 25 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 26 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 27 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 28 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 29 | 60 |
| [@ahegyi](https://gitlab.com/ahegyi) | 30 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 31 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 32 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 33 | 40 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 34 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 35 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 36 | 30 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 39 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 40 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 41 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 40 |
| [@fneill](https://gitlab.com/fneill) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@trakos](https://gitlab.com/trakos) | 1 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |



---
layout: handbook-page-toc
title: "Many decisions — Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/many-decisions/"
description: Many decisions — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The ability for an organization to make **many** decisions is a key attribute of [MECC](/handbook/mecc/). Conventional management philosophies often strive for consensus and avoid risk instead of a [bias for action](/handbook/values/#bias-for-action), resulting in ***less frequent*** decision making. When you're operating in a MECC mindset, success is measured by the ***quantity*** of decisions made in a particular stretch of time (e.g. month, quarter) and the results that stem from faster progress. 

Below are the tenets of Many Decisions within MECC. 

## Boring solutions 

Traditional organizations often place merit in proposing the most cutting-edge, complex, or interesting solution to solve a problem. Instead, MECC means choosing ["boring" or simple solutions](/handbook/values/#boring-solutions). By taking every opportunity to reduce complexity in the organization, you're able to increase the speed and frequency of innovation. 

Embracing boring solutions and shipping the [minimum viable change (MVC)](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) also means [accepting mistakes](/handbook/values/#accept-mistakes) if that solution doesn't work, and moving on to the next [iteration](/handbook/values/#iteration). Avoiding an unnecessarily complicated first decision ensures that mistakes are far less costly, allowing for ***more*** decision-making in a ***shorter*** span of time. 

**Here's an example:** Product releases

- _In a conventional organization:_ The product design team combines a long list of user-requested updates to the product, and works toward a solution that incorporates all of them in one release. Their determination to create a perfect, fancy solution causes the release to be delayed. Users and customers are frustrated that fixes and features are not released more frequently.     
- _In an organization empowered by MECC:_ The product design team at GitLab wanted to make it easier to find the title of an issue without scrolling back to the top of the page. [They considered](/blog/2020/08/18/boring-solutions-faster-iteration/#boring-can-also-make-work-easier) making other aspects of the page sticky at the same time, but decided to [cut scope](/handbook/values/#set-a-due-date) and release the boring solution and smallest iteration. This approach allows the team to ship features to customers in every monthly release, and embrace a ["don't wait"](/handbook/values/#dont-wait) mentality.

## Collaboration is not consensus

MECC unlocks your organization's potential for making many decisions by challenging the idea that consensus is productive. Organizations should strive to have smaller teams iterating quickly but transparently (allowing everyone to contribute), rather than a large team producing things slowly as they work toward consensus. 

This [permissionless innovation](/handbook/values/#collaboration-is-not-consensus) also requires leaders and managers to *not* let their desire to feel involved stifle their team's [bias for action](/handbook/values/#bias-for-action) and the number of decisions being made. If you choose the right [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) and empower them to work transparently, you should not expect them to wait for a brainstorming meeting or time on the calendar for group sign-off.   

**Here's an example:** Choosing a new vendor

- _In a conventional organization:_ When choosing a new analytics vendor, a large project team meets weekly to brainstorm and vet the available options. Once they finally narrow down to a few finalists, they struggle to find consensus across the group. This one decision takes multiple months, which hinders progress and additional decisions.  
- _In an organization empowered by MECC:_ When tasked with choosing a new analytics vendor, the project's DRI becomes informed about the available options in an issue that's [shared transparently](/handbook/values/#findability) with their broader team. The DRI consults and seeks input from other team members, but does *not* wait to hear from every person before making a decision. The vendor is selected, and the team moves forward to the next decision.  

## Say why, not just what

It can be tempting for a risk-intolerant organization to announce a change without much context in hopes that it will "fly under the radar" and avoid controversy. MECC organizations recognize that up-front transparency is a foundational element to more frequent decision-making. This requires you to not only share ***what*** the decision is, but also [***why***](/handbook/values/#say-why-not-just-what) it's being made. 

While saying "why" does not mean *justifying* every decision against other alternatives, it does require a DRI or leader to [articulate their reasoning](/handbook/values/#articulate-when-you-change-your-mind). This prevents speculation and [builds trust](/handbook/leadership/building-trust/), which is one of the traits of being a [great remote manager](/company/culture/all-remote/being-a-great-remote-manager/). Beyond these cultural benefits, saying "why" also creates institutional memory that is powerful for the future efficiency of your organization. When a related change is being made a year later, the new DRI will have access to the [well-documented](/company/culture/all-remote/handbook-first-documentation/#why-does-handbook-first-documentation-matter) reasoning behind past decisions. They're able to better understand the context, avoid duplicating work or mistakes, and make more consecutive decisions.

**Here's an example:** Updating a team member perk

- _In a conventional organization:_ The People Group announces that the company will no longer be offering one of their employee perks. They don't share the reasoning or the "why" behind the change to the offering, so team members begin to speculate. This is disruptive and causes a breakdown in trust and communication, and managers spend their time trying to make up for the lack of transparency. Without clear documentation or context, it's also difficult to make future decisions about the program or transfer knowledge as the team continues to scale.
- _In an organization empowered by MECC:_ The People Group realizes it's time to update a team member perk that has become difficult to manage as the team grows. At GitLab, we'd share a [merge request](/handbook/communication/#everything-starts-with-a-merge-request) that clearly explains what the change is as well as the reasoning behind it. Not every team member will agree with the outcome, but they can accept it because they understand the *why*. As the team continues to scale, [this level of transparency](/handbook/values/#transparency-is-most-valuable-if-you-continue-to-do-it-when-there-are-costs) and knowledge is also transferred to new hires, so there's enough context for [self-learning](/company/culture/all-remote/self-service/) and future iterations.

## Asynchronous workflows

Asynchronous work is about more than just freeing up calendar space. In MECC, asynchronous workflows enable time independence across the organization, allowing you to make more frequent, inclusive decisions. 

Instead of spending time scouring schedules and time zone differences to discuss something synchronously, shift the focus to creating clear documentation that will allow team members to contribute on their own time, and with more intentionality. This [gives agency](/handbook/values/#give-agency), reinforces a [bias for action](/handbook/values/#bias-for-action), and [bridges the knowledge gap](/company/culture/all-remote/asynchronous/#6-asynchronous-work-bridges-the-knowledge-gap), resulting in *more* total iterations. 

Establishing a thriving asynchronous culture also requires leaders to [celebrate incremental improvements](/company/culture/all-remote/asynchronous/#celebrate-incremental-improvements), encouraging their team to strive for [iteration](/handbook/values/#iteration) and [progress, not perfection](/company/culture/all-remote/asynchronous/#aim-for-progress-not-perfection). 

**Here's an example:** Handbook and process updates

- _In a conventional organization:_ A team is tasked with updating one of their processes, which is complicated because it's documented across several disparate tools. The first time they can find reasonable time zone alignment to meet about this is in 3 weeks, so they schedule it and hold off on making any decisions until everyone can be present on a call.  
- _In an organization empowered by MECC:_ A team member opens an issue to highlight several needed updates to their handbook page and issue templates. The team is able to make lots of small decisions (via commits and merge requests at GitLab; via an org-wide tool elsewhere) in just a few days by collaborating on a dynamic schedule. When they have their next [synchronous meeting](/company/culture/all-remote/meetings/), they've already made significant progress. They [attach an agenda](/company/culture/all-remote/meetings/#4-all-meetings-must-have-an-agenda) ahead of time and record the session for async participation by those [not in attendance](/company/culture/all-remote/meetings/#1-make-meeting-attendance-optional).  

## Only healthy constraints 

Organizational growth does not have to result in stagnation. This component of MECC requires leaders to rethink the conventional processes and regulations that come with scaling a company, and resist the assumption that slower pace and less decision-making is inevitable. 

Allowing [only healthy constraints](/handbook/values/#only-healthy-constraints) enables an organization to continue to operate with the agility of a startup while realizing the efficiencies of a scaling company.

**Here's an example:** Scaling the team

- _In a conventional organization:_ As the team grows, new processes and approvals are put in place because it's how other "mature" companies operate. This creates a slower pace of progress (e.g. [Slime Molds](https://komoroske.com/slime-mold/)), reducing the number of decisions made. Management accepts that this is just a natural part of becoming a larger company, and does not challenge or question whether the additional bureaucracy is necessary. 
- _In an organization empowered by MECC:_ As the team grows, they commit to actively addressing and removing unnecessary blocks to progress and iteration, even rewarding team members for challenging constraints. At GitLab, we've documented [more than 15 ways](/handbook/only-healthy-constraints/#resisting-unhealthy-constraints) we resist unhealthy constraints. 


---

Making many decisions enables you to [execute on decisions](/handbook/mecc/executing-decisions/), the fourth tenet of [MECC](/handbook/mecc/). 

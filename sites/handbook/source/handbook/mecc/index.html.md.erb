---
layout: handbook-page-toc
title: "Managing so Everyone Can Contribute (MECC)"
canonical_path: "/handbook/mecc/"
description: GitLab Management Philosophy — Managing so Everyone Can Contribute (MECC)
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

Managing so Everyone Can Contribute (MECC) is a new management philosophy to speed up company progress by creating the environment for better decisions and improved execution of them. GitLab pioneered MECC and continues to steward its development. 

MECC succeeds by enabling the following.

1. [Informed decisions](/handbook/mecc/informed-decisions/)
   - The key question answered is no longer, "How does the organization get information to the right people at the right time?", but rather, "How does the organization ***create a system*** where everyone can consume information (self-serve) and contribute, regardless of level or function?"
1. [Fast decisions](/handbook/mecc/fast-decisions/)
   - While other management philosophies prioritize the speed of knowledge ***transfer***, MECC optimizes for the speed of knowledge ***retrieval***.
1. [Many decisions](/handbook/mecc/many-decisions/)
   - Conventional management philosophies often strive for consensus and avoid risk instead of a [bias for action](/handbook/values/#bias-for-action), resulting in ***less frequent*** decision making. When you're operating in a MECC mindset, success is measured by the ***quantity*** of decisions made in a particular stretch of time (e.g. month, quarter) and the results that stem from faster progress.
1. [Executing on decisions](/handbook/mecc/executing-decisions/)
   - Being more informed, making faster decisions, and increasing decision making velocity is only useful if you execute. MECC empowers ***everyone*** to contribute to successful execution. It also recognizes that execution ***is not a one-time event***; rather, it is the establishing of a new baseline on which future [iterations](/handbook/values/#iteration) are applied.

## MECC Vision

It is GitLab's [mission](/company/mission/) to make it so that **everyone can contribute**. When applied to management, this creates an atmosphere where everyone is empowered to lead. 

MECC differentiates itself from other management philosophies by consciously enabling decentralized decision making at a centralized (organizational) level. While guiding principles exist, it is not static. It is designed to be iterated on and evolved by everyone. The underpinnings of its philosophy are designed to apply to **all** work environments, from [no remote to strictly remote](/company/culture/all-remote/stages/). 

By implementing MECC's unique foundational principles at an *organizational* level, *individuals* within the organization are less constrained. Each team member receives greater agency to exert self-leadership. Collectively, we believe this atmosphere allows for more informed decisions, made quicker, more frequently, and with a higher likelihood of successful execution. 

MECC is a recipe which has worked at GitLab. It may not be perfectly applicable in your company, and that's OK. As with [The Remote Playbook](https://learn.gitlab.com/allremote/), we are transparently sharing it to inspire other organizations and to invite conversation. 

## Building upon all-remote

GitLab pioneered the [all-remote workplace model](/company/culture/all-remote/). MECC represents the ultimate phase of [remote adaptation](/company/culture/all-remote/phases-of-remote-adaptation/) (Skeuomorph → Functional → Asynchronous → Intentionality → MECC). Two examples are below.

1. All-remote enables GitLab to hire the world's best talent. This leads to significant time zone gaps amongst teams. MECC turns this challenge into opportunity through the [Manager of One](/handbook/leadership/#managers-of-one) attribute, which empowers each team member — regardless of level in the company — to act with [agency](/handbook/values/#give-agency).
1. All-remote enables team members to [structure their days dynamically](/company/culture/all-remote/non-linear-workday/). This makes it difficult or impossible to tap someone on the shoulder for information. MECC turns this challenge into opportunity by instilling rigor around documentation, which GitLab refers to as [handbook-first](/handbook/handbook-usage/#why-handbook-first). This enables knowledge to scale, leading to greater [efficiency](/handbook/values/#efficiency) in decision making.  

While all-remote created ideal conditions to develop and evolve MECC at GitLab, MECC can be applied to colocated and hybrid-remote organizations as well. 

If applying MECC in a hybrid-remote environment, be aware of [common hybrid pitfalls](/company/culture/all-remote/hybrid-remote/) and take time to understand the [differences between all-remote and hybrid-remote](/company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/).

## Understanding and applying MECC

1. **MECC describes an ideal state**. In management, it is not possible to remain in an ideal state in perpetuity. Competing priorities, conflict tradeoffs, and [coordination headwinds](https://komoroske.com/slime-mold/) will be present at varying times. When applying MECC, resist the urge to take a binary approach. Rather than asking, "Have we completely achieved MECC in our team or company?," leverage MECC principles to *navigate* through the aforementioned priorities, tradeoffs, and headwinds with [more information](/handbook/mecc/informed-decisions/) and [greater velocity](/handbook/mecc/fast-decisions/). 
1. **MECC is for individual contributors *and* people managers.** The term "Managing" in "Managing so Everyone Can Contribute" is not to be conflated with the term "People Manager." Every contributor, including an individual contributor, is a [Manager of One](/handbook/leadership/#managers-of-one). MECC empowers individual contributors to be better stewards of their own time and attention. MECC empowers people managers to lead with deeper conviction while creating more space for their direct reports to grow, develop, and contribute.
